import sys
from distutils.core import setup

import sys
from distutils.core import setup

if 'py2exe' in sys.argv:
    import py2exe

    # use windows for GUI
    #kwargs = dict(windows=[{'script': 'tank-model.py', 'dest_base': 'tank-model'}],
    kwargs = dict(console=[{'script': 'tank-model.py', 'dest_base': 'tank-model'}],
                  options={'py2exe': dict(optimize=2, compressed=1, bundle_files=1)},
                  zipfile=None)
else:
    kwargs = dict(scripts=['tank-model.py'])

setup(name='tank-model', **kwargs)



