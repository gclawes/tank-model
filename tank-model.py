#!/usr/bin/env python
"""
tank-model.py
Man run-time for tank-model color converter
Runs either gui.run() or cli.run() based on arguments
"""
__author__      = "Graeme Lawes"
__credits__     = "Gregory Lawes"

__version__     = "0.1"
__maintainer__  = "Graeme Lawes"
__email__       = "gclawes@udel.edu"
__status__      = "Development"

import sys
from optparse import OptionParser

def main(argv):
    """
    Decides between cli and gui based on presence of --cli argument
    """
    parser = OptionParser(usage="Usage: %prog --cli [OPTIONS] infile outfile")
   
    # cli interface
    parser.add_option("--cli", action='store_true', dest='cli', \
                      help="Use command-line interface", default=False)
    # min/max ranges
    parser.add_option("--range", action='store', nargs=2, dest='set_range', \
                      metavar='MIN MAX', help="Set min/max range from tank center axis")
    # intensity range
    parser.add_option("--intensity", action='store', nargs=2, dest='intensity_range', \
                      metavar='MIN MAX', help="Set min/max for color intensity")

    # use HSV 
    parser.add_option("--rgb", action='store_true', dest='rgb',\
                      help="Ouptut RGB values instead of intensity", default=False)

    # hue range
    parser.add_option("--hue", action='store', nargs=2, dest='hue_range', \
            metavar='MIN MAX', help="Set min/max for HSV hue. Default: 0-120")


    (opts, args) = parser.parse_args()

    if opts.cli:
        import cli
        cli.run(opts,args, parser)
    else:
        print "GUI not implemented"
        parser.print_help()
    


if __name__ == "__main__":
    main(sys.argv)

