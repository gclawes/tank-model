#!/usr/bin/python

# TODO:
# 1. Fix formula - DONE
# 2. drop bad lines - DONE
# 3. dynamic output range - DONE
# 4. min/max selection - DONE
# 5. min/max outlier elimination - NOT NOW


import sys
import csv
from math import sqrt

# detect OS for getch()
from platform import system
if system() == 'Windows':
    from msvcrt import getch


## GLOBAL VARIABLES ##

# field names for CSV file
fn=("x", "y", "z", "range")

# output min/max defaults
COLOR_MIN, COLOR_MAX = 0, 255

## EXCEPTIONS ##

class BadLineError(Exception):
    def __init__(self, line):
        self.line = line
    def __str__(self):
        return repr(self.line['x'] + " " + self.line['y'] + " " + self.line['z'] + " " + self.line['range'])

## FUNCTION DEFINITIONS ##

# takes 2 filenames
# returns 2 file descriptors
def open_files(iname, oname):
    try:
       infile = open(iname, 'rb')
    except IOError:
        print "Error: cannot open source file: " + iname
        sys.exit()
    
    try:
       outfile = open(oname, 'wb')
    except IOError:
        print "Error: cannot open destination file: " + oname
        sys.exit()

    return infile, outfile


# takes 2 file descriptors
# returns a tuple of the dictreader and dictwriter
def csv_setup(infile, outfile):
    dr = csv.DictReader(infile, delimiter=' ', fieldnames=fn)
    dw = csv.DictWriter(outfile, delimiter=' ', fieldnames=fn)

    return dr, dw

# asks if user wants to change calculated min/max (cmin/cmax)
def ask_min_max(cmin, cmax):
    response = raw_input("Do you wish to specify custom values instead? (y/n) [n]: ")
    if response == "y":
        try:
            new_min = float(raw_input("Enter new minimum: "))
            new_max = float(raw_input("Enter new maximum: "))
        except ValueError:
            print "Invalid number, try again"
            return ask_min_max(cmin, cmax)
        if new_min >= new_max:
            print "Invalid range, try again"
            return ask_min_max(cmin, cmax)
        else:
            return new_min, new_max
    else:
        return cmin, cmax

# calculate min/max, and ask user if they want to specify another range
def min_max(infile):
    infile.readline()
    dr = csv.DictReader(infile, delimiter=' ', fieldnames=fn)
    
    amin = float('inf')
    amax = 0

    print "Calculating absolute minimum and maximum..."

    while True:
        try:
            line = dr.next()
        except StopIteration:
            break
        
        try:
            range = convert(line)
        except BadLineError as e:
            print "min_max: Ignoring bad line: ", e

        if range < amin:
            amin = range
        elif range > amax:
            amax = range

    infile.seek(0,0)

    print "Absolute minimum: " + str(amin)
    print "Absolute maximum: " + str(amax)
    min, max = ask_min_max(amin, amax)

    return min, max

# takes 2 default values for range
# sets output intensity min/max
def get_out_range(default_min, default_max):
    print "Default color intensity range is " + str(default_min) + " - " + str(default_max)
    response = raw_input("Do you wish to specify custom values instead? (y/n) [n]: ")
    if response == "y":
        try:
            new_min = float(raw_input("Enter new minimum: "))
            new_max = float(raw_input("Enter new maximum: "))
        except ValueError:
            print "Invalid number, try again"
            return ask_min_max(default_min, default_max)
        if new_min >= new_max:
            print "Invalid range, try again"
            return ask_min_max(default_min, default_max)
        else:
            return new_min, new_max
    else:
        return default_min, default_max



# takes a line and calculates the range from 
# (0,0,0) to (x,y,z) of the line
def convert(l):
    try:
        x = float(l['x'])
        y = float(l['y'])
        z = float(l['z'])
    except ValueError:
        raise BadLineError(l)
    return sqrt( (0-x)**2 + (0-y)**2 )

# handy code adapted from 
# http://stackoverflow.com/questions/1969240/mapping-a-range-of-values-to-another
def make_interp(left_min, left_max, right_min, right_max):
    lrange = left_max - left_min
    rrange = right_max - right_min

    scale = float(rrange) / float(lrange)

    def interp_fn(value):
        return right_min + (value-left_min)*scale
    return interp_fn

def main(argv):
    # 2 args - infile and outfile
    # 1 arg - interactive
    #if len(argv) == 3 or len(argv) == 1):
    if len(argv) < 2:
        print "Usage: convert.py infile outfile"
        sys.exit();
  
    # no output specified, append .out to input name
    if len(argv) == 2:
        argv.append(argv[1] + ".out")

    # open files
    infile, outfile = open_files(argv[1], argv[2])
    
    # NOTE: this will exclude the first line of the file
    nline = infile.readline()
    print nline.split(" ")[0]+ " datapoints to process"
    outfile.write(nline)
    infile.seek(0,0)
    
    min, max = min_max(infile)
    print "Min range: " + str(min)
    print "Max range: " + str(max)
    infile.readline()

    # setup output range
    out_min, out_max = get_out_range(COLOR_MIN, COLOR_MAX)

    # setup CSV reader and writer
    dreader, dwriter = csv_setup(infile, outfile)

    # set up interpolator
    interp = make_interp(min, max, out_min, out_max)

    # loop through entire input, write out
    # TODO: update this, do conversion
    while True:
        # open file
        try:
            line = dreader.next()
        except StopIteration: # hit EOF
            print "I'm Done!"
            break
        
        try:
            dist = convert(line)
            if dist < min:
                line['range'] = 0
            elif dist > max:
                line['range'] = 255
            else:
                line['range'] = str(int(round(interp(dist))))
            dwriter.writerow(line)
        except BadLineError as e:
            print "convert: Dropped bad line: ", e
        
    infile.close()
    outfile.close()
    
    if system() == 'Windows':
        print "Press any key to exit..."
        getch()


## RUN PROGRAM ##

if __name__ == "__main__":
    print "Welcome!"
    main(sys.argv)
