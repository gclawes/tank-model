"""
cli.py
Command-line interface for tank-modeling software
provides cli.run() to tank-model.py
"""
__author__      = "Graeme Lawes"
__credits__     = "Gregory Lawes"

__version__     = "0.1"
__maintainer__  = "Graeme Lawes"
__email__       = "gclawes@udel.edu"
__status__      = "Development"



import sys
from modules import convert, files
#from convert import BadLineError, BadHeaderError

# detect OS for getch()
from platform import system
if system() == 'Windows':
    from msvcrt import getch

def run(opts, args, parser):
    # check if there are no arguments after -c
    if len(args) == 0:
        parser.print_help()
        sys.exit()
    
    # args[0] is the input file, args[1] is the output name
    in_filename = args[0]
    input_reader, in_file = files.import_src(in_filename)
    
    # if no output file is specified, create one with a default name
    if len(args) < 2:
        out_filename = args[0]+".out"
    else:
        out_filename = args[1]
    out_writer, out_file = files.create_writer(out_filename)

    print "Input file: %s" % in_filename
    print "Output file: %s\n" % out_filename

    if opts.set_range:
        range_min = float(opts.set_range[0])
        range_max = float(opts.set_range[1])
    else:
        print "Calculating min/max range from tank center axis."
        print "This may take some time..."
        range_min, range_max = convert.calc_min_max(input_reader)
        in_file.seek(0)
    print "Minimum: %s" % range_min
    print "Maximum: %s\n" % range_max

    if opts.intensity_range:
        intensity_min = int(opts.intensity_range[0])
        intensity_max = int(opts.intensity_range[1])
    else:
        intensity_min = 0
        intensity_max = 255
    
    if opts.rgb:
        if opts.hue_range:
            h_min = int(opts.hue_range[0])
            h_max = int(opts.hue_range[1])
        else:
            h_min = 0
            h_max = 120
        interpolation = convert.Interpolation(range_min, range_max, \
                                        h_min, h_max, color=True)
    else:
        interpolation = convert.Interpolation(range_min, range_max, \
                                        intensity_min, intensity_max)
   
    print "Running converson"
    run_conversion((input_reader, in_file), \
                   (out_writer, out_file), 
                   interpolation)

def run_conversion(input, output, i):
    """
    takes the input/output csv objects, the distance and intensity ranges
    run through the input DictReader line by line, and
    writes a new file with the calculated intensity values

    """
    reader, in_file = input
    writer, out_file = output
    #range_min, range_max = range
    #intensity_min, intensity_max = intensity
    
    # parse header, get number of entries
    entries = convert.parse_header(in_file.readline())
    print "%d entries to process.\n" % entries
    out_file.write(str(entries)+"\n")

    for line in reader:
        try:
            writer.writerow(i.interpolate_line(line))
        except convert.BadLineError as e:
            try:
                new_entries = convert.parse_header(line['x'])
                print "New header found: %d entries" % new_entries
            except convert.BadHeaderError:
                print "run_conversion: Ignoring bad line: ", e

    in_file.close()
    out_file.close()

    if system() == 'Windows':
        print "Press any key to exit..."
        getch()
