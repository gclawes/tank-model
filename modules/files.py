"""
Read CSV file representing tank 
scans and write processed output
"""
__author__      = "Graeme Lawes"
__credits__     = "Gregory Lawes"

__version__     = "0.1"
__maintainer__  = "Graeme Lawes"
__email__       = "gclawes@udel.edu"
__status__      = "Development"


import csv
import sys
from exceptions import Exception


## GLOBAL VARIABLES ##

# field names for CSV file
pts_fields=("x", "y", "z", "range")
color_fields=("x", "y", "z", "rgb")

# output min/max defaults
COLOR_MIN, COLOR_MAX = 0, 255

## EXCEPTIONS ##

class OpenSrcError(Exception):
    def __init__(self, filename):
        self.filename = filename
    def __str__(self):
        return "Error opening source file %s for reading" % str(self.filename)

class OpenOutError(Exception):
    def __init__(self, filename):
        self.filename = filename
    def __str__(self):
        return "Error opening source file %s for writing" % str(self.filename)

def open_src(in_name):
    try:
        infile = open(in_name, 'rb')
    except:
        print "Error opening input file: %s" % in_name
        #raise OpenSrcError(in_name)
        sys.exit()
    return infile 

def open_output(out_name):
    try:
        file = open(out_name, 'wb')
    except:
        raise OpenOutError(out_name)
        return
    return file 

def import_src(input_name):
    """
    imports a source file as a csv.dictreader iterable object
    also returns the input file 
    """
    infile = open_src(input_name)
    reader = csv.DictReader(infile, delimiter=' ', fieldnames=pts_fields)
    return reader, infile

def create_writer(output_name):
    """creates a csv.dictwriter for the output file, returns it"""
    out_file = open_output(output_name)
    writer = csv.DictWriter(out_file, delimiter=' ', fieldnames=pts_fields)
    return writer, out_file
