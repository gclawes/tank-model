"""
run conversions on .pts files
"""
__author__      = "Graeme Lawes"
__credits__     = "Gregory Lawes"

__version__     = "0.1"
__maintainer__  = "Graeme Lawes"
__email__       = "gclawes@udel.edu"
__status__      = "Development"


from math import sqrt
from colorsys import hsv_to_rgb

SATURATION = 1
VALUE = 1

## EXCEPTIONS ##

class BadLineError(Exception):
    def __init__(self, line):
        self.line = line
    def __str__(self):
        return "%s %s %s %s" % (str(self.line['x']), str(self.line['y']), \
                str(self.line['z']), str(self.line['range']))

class BadHeaderError(Exception):
    def __init__(self, line):
        self.line = line
    def __str__(self):
        return "Bad Header: %s" % str(self.line)

def color_float(color_min, color_max):
    color_range = color_max - color_min
    scale = 1.0 / float(color_range) 
    def interp_fn(value):
        return (value-color_min)*scale
    return interp_fn
    
def interpolation(left_min, left_max, right_min, right_max):
    """
    create interpolation function to map range (left_min, left_max)
    to (right_min, right_max).  Used for mapping range of distance to
    range of intensity values).
    handy code adapted from 
    http://stackoverflow.com/questions/1969240/mapping-a-range-of-values-to-another
    """
    lrange = left_max - left_min
    rrange = right_max - right_min
    
    scale = float(rrange) / float(lrange)
    
    def interp_fn(value):
        return right_min + (value-left_min)*scale
    return interp_fn



class Interpolation:
    def __init__(self, left_min, left_max, right_min, right_max, color=False):
        self.interpolate = interpolation(left_min, left_max, \
                right_min, right_max)
        self.color = color
        self.range_min = left_min
        self.range_max = left_max
        self.intensity_min = right_min
        self.intensity_max = right_max
        self.color_interp = color_float(right_min, right_max)

    def interpolate_line(self, line):
        range = calculate_range(line)
        if range < self.range_min: temp = self.intensity_min
        elif range > self.range_max: temp = self.intensity_max
        else: temp = int(self.interpolate(range))
       
        if self.color:
            rgb = [str(float(v)) for v in hsv_to_rgb(self.color_interp(temp), SATURATION, VALUE)]
            #print "%s\t%s" % (self.color_interp(temp), rgb)
            line['range'] = ','.join(rgb)
        else:
            line['range'] = str(temp)
        return line


def calculate_range(line):
    """takes a line from a DictReader, calculates the 
    range from center axis to tank side"""
    try:
        x = float(line['x'])
        y = float(line['y'])
        z = float(line['z'])
    except ValueError:
        raise BadLineError(line)
    return sqrt( (0-x)**2 + (0-y)**2 )

def parse_header(line):
    try:
        return(int(line))
    except:
        raise BadHeaderError(line)
        return

def calc_min_max(file_reader):
    """
    Calculates the min/max based on filename for an input file
    """
    min = float('inf')
    max = 0

    for line in file_reader:
        try:
            range = calculate_range(line)
            if range < min:
                min = range
            elif range > max:
                max = range

        except BadLineError as e:
            pass

    
    return min, max
